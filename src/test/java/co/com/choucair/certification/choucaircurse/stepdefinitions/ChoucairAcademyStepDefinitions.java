package co.com.choucair.certification.choucaircurse.stepdefinitions;

import co.com.choucair.certification.chourcaircurse.model.AcademyChoucairData;
import co.com.choucair.certification.chourcaircurse.questions.Answer;
import co.com.choucair.certification.chourcaircurse.tasks.Login;
import co.com.choucair.certification.chourcaircurse.tasks.OpenUp;
import co.com.choucair.certification.chourcaircurse.tasks.Search;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;


public class ChoucairAcademyStepDefinitions {

    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^than Michael wants to learn automation at the academy Choucair$")
    public void thanMichaelWantsToLearnAutomationAtTheAcademyChoucair(List<AcademyChoucairData> academyChourcairData) throws Exception {
        OnStage.theActorCalled("Michael").wasAbleTo(OpenUp.thePage(),(Login.
                onThePage(academyChourcairData.get(0).getStrUser(),academyChourcairData.get(0).getStrPassword())));

    }

    @When("^he search for the course  on the choucair academy platform$")
    public void heSearchForTheCourseMeansAutomationBancolombiaOnTheChoucairAcademyPlatform( List<AcademyChoucairData> academyChourcairData) throws Exception {
        OnStage.theActorInTheSpotlight().attemptsTo(Search.the(academyChourcairData.get(0).getStrCourse()));
    }

    @Then("^he finds the course called resources $")
    public void heFindsTheCourseCalledResourcesMeansAutomationBancolombia(List<AcademyChoucairData> academyChourcairData) throws Exception {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(academyChourcairData.get(0).getStrCourse())));

    }
}
